#!/bin/bash

cat system/system/priv-app/AsusSettings/AsusSettings.apk.* 2>/dev/null >> system/system/priv-app/AsusSettings/AsusSettings.apk
rm -f system/system/priv-app/AsusSettings/AsusSettings.apk.* 2>/dev/null
cat system/system/priv-app/AsusCamera/AsusCamera.apk.* 2>/dev/null >> system/system/priv-app/AsusCamera/AsusCamera.apk
rm -f system/system/priv-app/AsusCamera/AsusCamera.apk.* 2>/dev/null
cat system/system/priv-app/YandexBrowser/YandexBrowser.apk.* 2>/dev/null >> system/system/priv-app/YandexBrowser/YandexBrowser.apk
rm -f system/system/priv-app/YandexBrowser/YandexBrowser.apk.* 2>/dev/null
cat system/system/priv-app/YandexApp/YandexApp.apk.* 2>/dev/null >> system/system/priv-app/YandexApp/YandexApp.apk
rm -f system/system/priv-app/YandexApp/YandexApp.apk.* 2>/dev/null
cat system/system/priv-app/ZF8LiveWallpaper/ZF8LiveWallpaper.apk.* 2>/dev/null >> system/system/priv-app/ZF8LiveWallpaper/ZF8LiveWallpaper.apk
rm -f system/system/priv-app/ZF8LiveWallpaper/ZF8LiveWallpaper.apk.* 2>/dev/null
cat system/system/etc/preload/com.google.android.apps.photos.apk.* 2>/dev/null >> system/system/etc/preload/com.google.android.apps.photos.apk
rm -f system/system/etc/preload/com.google.android.apps.photos.apk.* 2>/dev/null
cat system/system/app/MobilePASMO/MobilePASMO.apk.* 2>/dev/null >> system/system/app/MobilePASMO/MobilePASMO.apk
rm -f system/system/app/MobilePASMO/MobilePASMO.apk.* 2>/dev/null
cat system/system/app/AsusSetupWizard/AsusSetupWizard.apk.* 2>/dev/null >> system/system/app/AsusSetupWizard/AsusSetupWizard.apk
rm -f system/system/app/AsusSetupWizard/AsusSetupWizard.apk.* 2>/dev/null
cat system/system/app/NextAppCore/lib/arm64/libnd4jcpu.so.* 2>/dev/null >> system/system/app/NextAppCore/lib/arm64/libnd4jcpu.so
rm -f system/system/app/NextAppCore/lib/arm64/libnd4jcpu.so.* 2>/dev/null
cat vendor_boot.img.* 2>/dev/null >> vendor_boot.img
rm -f vendor_boot.img.* 2>/dev/null
cat boot.img.* 2>/dev/null >> boot.img
rm -f boot.img.* 2>/dev/null
cat system_ext/apex/com.android.vndk.v30.apex.* 2>/dev/null >> system_ext/apex/com.android.vndk.v30.apex
rm -f system_ext/apex/com.android.vndk.v30.apex.* 2>/dev/null
cat vendor_bootimg/09_dtbdump_x~,seU:9nG!+.dtb.* 2>/dev/null >> vendor_bootimg/09_dtbdump_x~,seU:9nG!+.dtb
rm -f vendor_bootimg/09_dtbdump_x~,seU:9nG!+.dtb.* 2>/dev/null
cat product/priv-app/Velvet/Velvet.apk.* 2>/dev/null >> product/priv-app/Velvet/Velvet.apk
rm -f product/priv-app/Velvet/Velvet.apk.* 2>/dev/null
cat product/priv-app/GmsCore/GmsCore.apk.* 2>/dev/null >> product/priv-app/GmsCore/GmsCore.apk
rm -f product/priv-app/GmsCore/GmsCore.apk.* 2>/dev/null
cat product/app/Messages/Messages.apk.* 2>/dev/null >> product/app/Messages/Messages.apk
rm -f product/app/Messages/Messages.apk.* 2>/dev/null
cat product/app/WebViewGoogle/WebViewGoogle.apk.* 2>/dev/null >> product/app/WebViewGoogle/WebViewGoogle.apk
rm -f product/app/WebViewGoogle/WebViewGoogle.apk.* 2>/dev/null
cat product/app/TrichromeLibrary/TrichromeLibrary.apk.* 2>/dev/null >> product/app/TrichromeLibrary/TrichromeLibrary.apk
rm -f product/app/TrichromeLibrary/TrichromeLibrary.apk.* 2>/dev/null
cat product/app/YouTube/YouTube.apk.* 2>/dev/null >> product/app/YouTube/YouTube.apk
rm -f product/app/YouTube/YouTube.apk.* 2>/dev/null
cat product/app/Gmail2/Gmail2.apk.* 2>/dev/null >> product/app/Gmail2/Gmail2.apk
rm -f product/app/Gmail2/Gmail2.apk.* 2>/dev/null
cat product/app/LatinImeGoogle/LatinImeGoogle.apk.* 2>/dev/null >> product/app/LatinImeGoogle/LatinImeGoogle.apk
rm -f product/app/LatinImeGoogle/LatinImeGoogle.apk.* 2>/dev/null
cat product/app/Maps/Maps.apk.* 2>/dev/null >> product/app/Maps/Maps.apk
rm -f product/app/Maps/Maps.apk.* 2>/dev/null
