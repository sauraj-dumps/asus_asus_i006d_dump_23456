## qssi-user 12 SKQ1.210821.001 31.1004.0404.73 release-keys
- Manufacturer: asus
- Platform: lahaina
- Codename: ASUS_I006D
- Brand: asus
- Flavor: qssi-user
- Release Version: 12
- Id: SKQ1.210821.001
- Incremental: 31.1004.0404.73
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: true
- Locale: en-US
- Screen Density: undefined
- Fingerprint: asus/WW_I006D/ASUS_I006D:11/RKQ1.210303.002/31.1004.0404.28:user/release-keys
- OTA version: 
- Branch: qssi-user-12-SKQ1.210821.001-31.1004.0404.73-release-keys
- Repo: asus_asus_i006d_dump_23456


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
